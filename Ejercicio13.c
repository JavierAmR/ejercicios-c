//13. Write a program that asks for a sequence of numbers to the user and show the sum of all of them. Process stops when a negative number is introduced.

#include <stdio.h>

int main ()
{
        //Utilizamos enteros ya que seguramente no se usen decimales
        int sum = 0;
        int prdt = 1;
        int entrada;
        
        //Creamos el bucle de la secuencia con la condición de salida
        while (entrada >= 0)
        {
            printf ("Introduce el valor que desea sumar a la secuencia, si es negativo se finalizara el proceso: \n");        
            scanf ("%i", &entrada);
            getchar ();
            //Creamos la condición de suma a la secuencia
            if (entrada >= 0)
            {
                    sum += entrada;
            }
            
        }
        
        
        printf ("La suma total de la secuencia es: %d\n",sum);
        getchar();
        return 0;
}