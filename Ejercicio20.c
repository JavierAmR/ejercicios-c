/*20. Write a calculator program. Program flow should be as follow:
1) Ask for a number (float)
2) Ask for desired operation: '+', '-', '*', '/'
3) Ask for another number (float)
4) Show operation and results
5) Go to step 2) */


#include <stdio.h>
#include <stdlib.h>



int main ()
{
    //Utilizaremos enteros para evitar los decimales
    int num1;
    int num2;
    int resultado;
    //opcion podria declararse como un enum para ayudar a entender el programa
    int opcion;
    
    //Creamos el bucle de funcionamiento de la calculadora
    do
    {
        printf ("Introduce el numero que quieres operar: \n");
        scanf ("%i", &num1);
        getchar();
        
        //Creamos un bucle para introducir solo las operaciones válidas
        do
        {
            printf ("Que operacion quieres realizar: 1=+ 2=- 3=* 4=/ 5=Salir\n");
            scanf ("%i", &opcion);
            getchar();
            
            if (opcion > 5 || opcion <= 0)
            {
                printf ("Esa opción no es valida, se le volvera a pedir que introduzca un numero\n");
            }
        } while (opcion > 5 || opcion <= 0);
        
        //Si no se pulsa 5 se pedira el segundo numero
        if (opcion != 5)
        {
            printf ("Introduce el segundo numero: \n");
            scanf ("%i", &num2);
            getchar();
        }
        
        //Depende del valor de ocpion utilizaremos un switch case para las distintas operaciones
        switch (opcion)
        {
            case 1: resultado = num1 + num2;
                break;
            case 2: resultado = num1 - num2;
                break;
            case 3: resultado = num1 * num2;
                break;
            case 4: resultado = num1 / num2;
                break;
        }  
        
        //Comprobación para que no salga el mensaje en caso de querer salir
        if (opcion != 5)
        {
            printf ("El resultado es: %i\n", resultado);
        }
    }while (opcion != 5);
    
   getchar();
   return 0;
}