//09. Write a program that asks for seconds and convert it into minutes, hours and days

#include <stdio.h>

int main ()
{
        //Utilizamos float ya que en medidas de tiempo se suelen usar bastantes decimales
        float s;
        float m;
        float h;
        float d;
        
        //Creamos un bucle de entrada para evitar valores erroneos
        do
        {
        printf ("Introduce los segundos que deseas convertir: \n");
        scanf ("%f", &s);
        getchar();
        
        if (s<0)
        {
            printf ("El valor introducido es menor a 0, se le volvera a pedir que lo introduzca\n")
        }
        } while (s<0);
        
        //Calculos de conversión a minutos, horas y dias
        m = s/60;
        h = m/60;
        d = h/24;
        
        printf ("El valor en minutos es: %fm\nEl valor en horas es: %fh\nEl valor en dias es: %fd\n",s,m,d);
        getchar();
        return 0;
}