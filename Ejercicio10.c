//10. Write a program that asks for 2 angles of a triangle and find the third angle.

#include <stdio.h>

int main ()
{
        //Utilizamos float ya que en angulos se suelen usar decimales
        float a1;
        float a2;
        float a3;
               
        //Creamos bucles para las entradas que impidan valores erroneos
        //Bucle en caso de que los dos angulos sumen 180
        do{
            //Bucle de angulo 1
            do
            {
                    printf ("Introduce un angulo del triangulo: \n");        
                    scanf ("%f", &a1);
                    getchar ();                    
                    if(a1 > 180 || a1 <0)
                    {
                        printf ("El valor introducido es mayor que 180 o menor que 0, vuelva a introducir un valor valido para un triangulo: \n");
                    }
            } while (a1 > 180 || a1 < 0);
            
            //Bucle de angulo 2      
            do
            {
                    printf ("Introduce el otro angulo del triangulo: \n");        
                    scanf ("%f", &a2);
                    getchar ();                    
                    if(a2 > 180 || a2 <0)
                    {
                        printf ("El valor introducido es mayor que 180 o menor que 0, vuelva a introducir un valor valido para un triangulo: \n");
                    }
            } while (a2 > 180 || a2 < 0);
            
            //Aviso si amboos angulos suman 180
            if (180 <= a2 + a1)
            {
                    printf ("Los valores suman 180 o mas por lo que es imposible que pertenezcan a un triangulo.\nSe volvera a pedir la introduccion de los valores\n");                
            }
        } while (180 <= a2 + a1);
        
        //Calculo del tercer angulo
        a3 = 180 - a1 - a2;
        
        printf ("El valor del tercer angulo es: %f\n",a3);
        getchar();
        return 0;
}