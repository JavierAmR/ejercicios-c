//05. Write a program that asks for radius of a circle and find its diameter, circumference and area.

#include <stdio.h>

int main ()
{
        //Utilizamos float ya que en medidas se suelen usar bastantes decimales
        float r;
        float d;
        float c;
        float a;
        
        //Realizamos un bucle del valor de entrada para evitar valores erroneos
        do
        {
            printf ("Introduce el radio: \n");
            scanf ("%f", &r);
            getchar();
            if (r <= 0)
            {
               printf ("El valor introducido es menor o igual a 0, se le volvera a pedir que lo introduzca\n");
            }
        } while (r <= 0);
        
        //Calculos para determinar el diametro, circunferencia y area
        d = 2 * r;
        c = 3.14 * d;
        a = r * r * 3.14;
        
        printf ("El diametro es: %f\nLa circunferencia es: %f\nEl area es: %f\n", d, c, a);
        getchar();
        return 0;
}