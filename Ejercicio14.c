//14. Write a program that asks for a sequence of numbers to the user and show the mean of all of them. Process stops only when number 0 is introduced.

#include <stdio.h>

int main ()
{
    //Utilizamos enteros ya que seguramente no se usen decimales
    int suma = 0;
    int media;
    int entrada;
    int i;
    
    printf ("Introduce numeros de uno en uno para calcular su media, cuando quieras parar introduce el numero 0: \n");
    
    //Bucle de valor de entrada con la condición de salida
    do 
    {
        scanf ("%i", &entrada);
        getchar();
        if (entrada != 0)
            {
                suma += entrada;
                //El valor i nos indicara la cantidad de numeros introducidos
                i++;
            }
    } while (entrada != 0);
    
    //Calculo de la media segun el valor de i que sera el total de numeros
    media = suma / i;
    
    printf ("La media es: %i\n", media);
    getchar();
    
    return 0;
}