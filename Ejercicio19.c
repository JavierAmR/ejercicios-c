//19. Write a function to calculate power of a value. Function declaration should be: int RaiseToPower(int value, int power);

#include <stdio.h>
#include <stdlib.h>

//Declaración de la funcion RaiseToPower
int RaiseToPower(int value, int power)
{
    int resultado = 1;
    
    //Bucle para calcular la potencia
    for (int i=0; i<power; i++)
    {
        resultado *= value;
    }
    
    return resultado;
}

int main ()
{
    //Utilizaremos enteros para evitar los decimales
    int entrada;
    int potencia;
    int resul;
    
    
    printf ("Introduce el numero que quieres utilizar: \n");
    scanf ("%i", &entrada);
    getchar();
    printf ("Introduce la potencia: \n");
    scanf ("%i", &potencia);
    getchar();
    
    //resul almacenara el valor ya elevado a la potencia
    resul = RaiseToPower (entrada, potencia);
    
    
    printf ("El resultado es: %i\n", resul);
    
    
   getchar();
   return 0;
}