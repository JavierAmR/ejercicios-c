//18. Write a function to check if an integer is negative. Function declaration should be: int IsNegative(int num);

#include <stdio.h>
#include <stdlib.h>

//Declaración de la funcion isNegative
int isNegative(int num)
{
    if (num >= 0)
    {
        return 0;
    }
    else 
    {
        return 1;
    }
}

int main ()
{
    //Creamos las variables de entrada y la que servira de almacenamiento de valor de retorno de isNegative
    int entrada;
    //Para hacerlo mas claro podriamos crear un bool con un enum
    int falsobool;
    
    
    printf ("Introduce el numero que quieres comprobar: \n");
    scanf ("%i", &entrada);
    
    //falsobool recibira el valor de retorno de isNegative
    falsobool = isNegative (entrada);
    
    //Segun el valor de falsebool escribiremos si es negativo o positivo
    if (falsobool == 1)
    {
         printf ("Es negativo\n");
    }
    else 
    {
        printf ("Es positivo\n");
    }
    
   getchar();
   return 0;
}