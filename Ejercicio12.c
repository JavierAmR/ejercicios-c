//12. Write a program that asks for 10 numbers to the user and show the sum and product of all of them. Use while loop.

#include <stdio.h>

int main ()
{
        //Utilizamos enteros ya que seguramente no se usen decimales
        int sum = 0;
        int prdt = 1;
        int i = 0;
        int entrada;
        
        //Creamos el bucle con la condición
        while (i<10)
        {
            printf ("Introduce el valor %i de la secuencia de 10 numeros: \n", i+1);        
            scanf ("%i", &entrada);
            getchar ();
            //En cada bucle recalculamos el producto y la suma con el nuevo valor
            prdt *= entrada;
            sum += entrada;
            i++;
        }
        
        
        printf ("La suma total de la secuencia es: %d\nEl producto total de la secuencia es: %d\n",sum, prdt);
        getchar();
        return 0;
}