//07. Write a program that asks for a temperature in Celsius(°C) and convert it into Fahrenheit(°F).

#include <stdio.h>

int main ()
{
        //Utilizamos float ya que en medidas se suelen usar bastantes decimales
        float c;
        float f;      
        
        printf ("Introduce el valor en grados Celsius: \n");
        scanf ("%f", &c);
        getchar();
        
        //Calculos para determinar el equivalente en grados Fahrenheit
        f = c*9/5+32;        
        
        printf ("El valor en grados Fahrenheit es: %fF\n",f);
        getchar();
        return 0;
}