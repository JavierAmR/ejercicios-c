//15. Write a program that asks for 1 number to the user and show its binary equivalent value. You can show the binary in inverted order.

#include <stdio.h>

int main ()
{
        //Usaremos enteros para poder usar de forma correcta el resto
        //Algo a destacar es que podriamos poner el orden correcto declarando un array con el numero maximo en binario y escribiendo los valores en binario en orden invertido
        int divisor;
        int resto;
        int entrada;
        
        //Creamos el bucle de entrada para evitar valores erroneos
        do{
        printf ("Introduce el numero a convertir a binario: \n");        
        scanf ("%i", &entrada);
        getchar ();
        
        if (entrada < 0)
            {
                printf ("El valor es menor que 0, se le volvera a pedir que introduzca el numero\n");
            }
        
        } while (entrada < 0);
        
        printf ("El numero en binario se mostrara en orden invertido: ");
        
        //Bucle de calculo del numero en binario
        while (entrada > 0)
        {
            resto = entrada % 2;
            entrada /= 2;
            printf ("%d", resto);            
        }
        printf ("\n"); 
        
        getchar ();
        return 0;
}