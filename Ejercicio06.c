//06. Write a program that asks for a value in centimeter and convert it into meter and kilometer.

#include <stdio.h>

int main ()
{
        //Utilizamos float ya que en medidas se suelen usar bastantes decimales
        float cm;
        float km;
        float m;        
        
        //Realizamos un bucle del valor de entrada para evitar valores erroneos
        do 
        {
            printf ("Introduce el valor en centimetros: \n");
            scanf ("%f", &cm);
            getchar();
            if (cm < 0)
            {
                printf ("El valor introducido es menor a 0, se le volvera a pedir que lo introduzca\n");
            }
        } while (cm < 0);
            
        //Calculos para determinar el equivalente en metros y kilometros de los centimetros
        m = cm / 100;
        km = m / 1000;
        
        printf ("El valor en metros es: %fm\nEl valor en kilometros es: %fkm\n",m ,km );
        getchar();
        return 0;
}