//09. Write a program that asks for seconds and convert it into minutes, hours and days

#include <stdio.h>

int main ()
{
    //Utilizamos enteros ya que no nos interesan los decimales
    int num1;
    int resultado;
    
    printf ("Indica el numero del que quieres saber la tabla de multiplicar:\n");
    scanf ("%i", &num1);
    getchar ();    
    
    printf ("La tabla de multiplicar es: \n");
    //Creamos un bucle for para escribir la tabla de multiplicar
    for (int i = 0; i<10; i++)
    {
        resultado = num1 * i;
        printf ("%i * %i = %i", num1, i, resultado);
    }
    
    getchar ();
    return 0;
}