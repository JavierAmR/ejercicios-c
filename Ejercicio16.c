//16. Write a program that that displays the first 100 prime numbers.

#include <stdio.h>

int main ()
{
        //Usaremos enteros ya que lo que buscamos son numeros primos
        int div = 0;
        int contador = 0;
        int i;
        
        printf ("Los 100 primeros numeros primos son:");

        //Creamos un bucle generador de numeros
        //Para optimizarlo mejor se podria usar la criba de Eratóstenes
        for (int j = 2; contador < 100; j++)
        {
            div = 0;
            i = 2;
            
            //Este bucle servira para comprobar si el numero generado es primo o no
            while (i <= j && div == 0)
            {
                if (j%i == 0 && j==i)
                {
                   printf (" %i ", j);
                   contador++;
                }
                else if (j%i == 0)
                {
                    div = 1;
                }
               i++;
            }
             
        }
        
        getchar ();
        return 0;
}